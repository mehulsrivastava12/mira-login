package com.example.demo.service;

import com.example.demo.entity.User;
import com.example.demo.repository.UserRepository;
import com.example.demo.util.EncodeDecode;
import com.example.demo.util.JwtUtil;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import java.security.SecureRandom;
import java.util.Base64;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;
    @Autowired
    JwtUtil jwtUtil;
    @Autowired
    EncodeDecode encodeDecode;

    @Value("${base.url}")
    private String baseUrl;

    private static final int TOKEN_LENGTH = 32;

    public String generateVerificationToken() {
        byte[] randomBytes = new byte[TOKEN_LENGTH];
        new SecureRandom().nextBytes(randomBytes);
        return Base64.getUrlEncoder().withoutPadding().encodeToString(randomBytes);
    }

    public User createUser(User user){
        User newUser = new User();
        newUser.setEmail(user.getEmail());
        newUser.setUserName(user.getUserName());
        newUser.setAccountVerified(false);
        newUser.setPassword(new BCryptPasswordEncoder().encode(user.getPassword()));
        newUser.setVerifyAccountToken(generateVerificationToken());
        String randomString = generateUniqueId();
        newUser.setUniqueId(randomString);
        return userRepository.save(newUser);
    }
    public ResponseEntity validateUser(String email, String password) {
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        User user = userRepository.findByEmail(email);
        boolean authenticated = false;
        if(user != null){
            if(user.getAccountVerified()){
                authenticated = encoder.matches(password, user.getPassword());
                if(authenticated){
                    return ResponseEntity.ok(user);
                }
                else {
                    return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Invalid Username or Password");
                }
            }
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body("User not found");
    }
    public String saveTokenForLoggedInUser(String accessToken,String loggedInToken) {
        boolean isValidUser = jwtUtil.validateToken(loggedInToken);
        if(isValidUser){
            User user = jwtUtil.extractUserDetailsFromToken(loggedInToken);
            if(user != null){
                accessToken = encodeDecode.encodeBase64(accessToken+user.getUniqueId());
                user.setAccessToken(accessToken);
                userRepository.save(user);
                return accessToken;
            }
        }
        return "";
    }
    public String generateUniqueId(){
        String randomString = null;
        boolean isNonUniqueStr = true;
        while (isNonUniqueStr) {
            randomString = RandomStringUtils.randomAlphanumeric(30);
            User existingUser = userRepository.findByUniqueId(randomString);
            if(existingUser == null){
                isNonUniqueStr = false;
            }
        }
        return randomString;
    }
}
