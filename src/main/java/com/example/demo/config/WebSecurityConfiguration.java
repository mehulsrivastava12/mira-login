package com.example.demo.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.client.RestTemplate;

@EnableWebSecurity
@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

    @Override
    public void configure(HttpSecurity http) throws Exception {
        http.cors().and().csrf().disable()
                .authorizeRequests(authorizeRequests ->
                        authorizeRequests
                                .antMatchers("/").permitAll()
                                .antMatchers("/login").permitAll()
                                .antMatchers("/signup").permitAll()
                                .antMatchers("/createUser").permitAll()
                                .antMatchers("/userLogout").permitAll()
                                .antMatchers("/verify").permitAll()
                                .antMatchers("/loginUser").permitAll()
                                .antMatchers("/oauth2/code").permitAll()// Allow access to the home page without authentication
                                .antMatchers("/serviceLogin").permitAll()// Allow access to the home page without authentication
                                .antMatchers("/crm").permitAll()// Allow access to the home page without authentication
                                .anyRequest().authenticated()
                )
                .oauth2Login(oauth2Login ->
                        oauth2Login
                                .loginPage("/login") // Specify the custom login page
                                .defaultSuccessUrl("/oauth2/code", true) // Specify the custom success URL
                )
                .logout(logout -> logout
                        .logoutUrl("/logout")
                        .logoutSuccessUrl("/")  // Specify the logout success URL
                        .clearAuthentication(true) // Clear the authentication after logout
                );
    }
}
