package com.example.demo.controller;

import com.example.demo.entity.User;
import com.example.demo.repository.UserRepository;
import com.example.demo.service.UserService;
import com.example.demo.util.JwtUtil;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@Controller
public class HomeController {

    @Autowired
    UserRepository userRepository;
    @Autowired
    UserService userService;
    @Autowired
    JwtUtil jwtUtil;
    @Autowired
    Environment environment;

    @Value("${base.url}")
    private String baseUrl;

    @RequestMapping("/")
    public String home() {
        return "home";
    }

    @RequestMapping("/signup")
    public String signUp() {
        return "signup";
    }

    @RequestMapping("/userLogout")
    public String logout(HttpSession session) {
        session.removeAttribute("token");
        return "redirect:/";
    }

    @RequestMapping("/login")
    public String login(@RequestParam(name = "source", required = false) String source, HttpSession session, Model model) {
        String loggedInToken = (String) session.getAttribute("token");
        if(loggedInToken == null && source == null){
            model.addAttribute("source",source);
            session.setAttribute("source",source);
            return "login";
        }
        else if(loggedInToken!=null && source == null) {
            return "userHome";
        }
        else if(loggedInToken!=null && source != null){
            String accessToken = RandomStringUtils.randomAlphanumeric(20);
            accessToken = userService.saveTokenForLoggedInUser(accessToken,loggedInToken);
            String propertyKey = "module." +source + ".url";
            String url = environment.getProperty(propertyKey);
            return "redirect:"+url+"?access_token="+accessToken;

        }
        else {
            model.addAttribute("source",source);
            session.setAttribute("source",source);
            return "login";
        }
    }
}
