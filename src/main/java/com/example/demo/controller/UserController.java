package com.example.demo.controller;

import com.example.demo.entity.User;
import com.example.demo.repository.UserRepository;
import com.example.demo.service.EmailService;
import com.example.demo.service.UserService;
import com.example.demo.util.EncodeDecode;
import com.example.demo.util.JwtUtil;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@Controller
public class UserController {
    @Autowired
    UserService userService;
    @Autowired
    EmailService emailService;
    @Autowired
    UserRepository userRepository;
    @Autowired
    JwtUtil jwtUtil;
    @Autowired
    EncodeDecode encodeDecode;

    @Autowired
    Environment environment;

    @Value("${base.url}")
    private String baseUrl;

    @PostMapping("/createUser")
    public String createUser(@ModelAttribute User user, Model model){
        if(user != null){
            User isUserAlreadyExist = userRepository.findByEmail(user.getEmail());
            if(isUserAlreadyExist != null){
                model.addAttribute("msg","User with this Email already Exist");
                return "signup";
            }
            User newUser = userService.createUser(user);
            if(newUser !=  null){
                emailService.sendEmail(newUser.getEmail(),newUser.getVerifyAccountToken());
                model.addAttribute("msg","Please check your email and verify your account");
                return "signup";
            }
        }
        model.addAttribute("msg","User cannot be created");
        return "signup";
    }

    @GetMapping("/verify")
    public String verifyAccount(@RequestParam("token") String token, Model model) {
        User user = userRepository.getByVerifyAccountToken(token);
        if (user != null) {
            user.setAccountVerified(true);
            userRepository.save(user);
            model.addAttribute("verificationResult", "Account verified successfully!");
            model.addAttribute("username", user.getUserName());
            return "verificationResult";
        } else {
            model.addAttribute("verificationResult", "Invalid verification token");
            return "verificationResult";
        }
    }

    @RequestMapping(path = "/loginUser",method = RequestMethod.POST)
    public String loginUser(@RequestParam("email") String email, @RequestParam("password") String password, @RequestParam(name = "source", required = false) String source, Model model, HttpSession session, RedirectAttributes redirectAttributes) {
        ResponseEntity authenticatedUser = userService.validateUser(email, password);
        if (authenticatedUser.getStatusCode() == HttpStatus.OK) {
            User user = userRepository.findByEmail(email);
            String token = jwtUtil.generateToken(user);
            session.setAttribute("token", token);
            if (source !=null && !source.equals("")) {
                String accessToken = RandomStringUtils.randomAlphanumeric(20);
                accessToken = userService.saveTokenForLoggedInUser(accessToken,token);
                String propertyKey = "module." +source + ".url";
                String url = environment.getProperty(propertyKey);
                return "redirect:"+url+"?access_token="+accessToken;
            } else {
                model.addAttribute("token",token);
                return "userHome";
            }
        }
        else if(authenticatedUser.getBody() == "Invalid Username or Password" && !source.equals("")){
            redirectAttributes.addFlashAttribute("msg", authenticatedUser.getBody());
            return "redirect:/login?source="+source;
        }
        else if(authenticatedUser.getBody() == "User not found" && !source.equals("")){
            redirectAttributes.addFlashAttribute("msg", "User with this email does not exist");
            return "redirect:/login?source="+source;
        }
        else if(authenticatedUser.getBody() == "User not found"){
            redirectAttributes.addFlashAttribute("msg", "User with this email does not exist");
            return "redirect:/login";
        }
        else {
            redirectAttributes.addFlashAttribute("msg", authenticatedUser.getBody());
            return "redirect:/login";
        }
    }

    @GetMapping("/oauth2/code")
    public String home(Model model, @AuthenticationPrincipal OAuth2User OAuth2User, HttpServletResponse response, HttpServletRequest request, HttpSession session) {
        String name = OAuth2User.getAttribute("name");
        String email = OAuth2User.getAttribute("email");
        User user = userRepository.findByEmail(email);
        String source = (String) session.getAttribute("source");
        String token = "";
        if(user == null){
            User newUser = new User();
            newUser.setEmail(email);
            newUser.setUserName(name);
            newUser.setAccountVerified(true);
            String randomString = userService.generateUniqueId();
            newUser.setUniqueId(randomString);
            User createdUser = userRepository.save(newUser);
            token = jwtUtil.generateToken(createdUser);
        }
        else {
            token = jwtUtil.generateToken(user);
        }
        session.setAttribute("token", token);
        model.addAttribute("token",token);
        if(source !=null){
            String accessToken = RandomStringUtils.randomAlphanumeric(20);
            accessToken = userService.saveTokenForLoggedInUser(accessToken,token);
            String propertyKey = "module." +source + ".url";
            String url = environment.getProperty(propertyKey);
            return "redirect:"+url+"?access_token="+accessToken;
        }
        return "redirect:/userhome";
    }

    @RequestMapping("/api/employee/authenticate/{accessToken}")
    public String validateAccessToken(@PathVariable String token) {
        User user = userRepository.findByAccessToken(token);
        token = encodeDecode.decodeBase64(token);
        String accessToken = token.substring(0, 20);
        String encodedUser = token.substring(20);
        if (user != null) {
            return encodedUser;
        } else {
            return null;
        }
    }

    @RequestMapping("/api/employee/user/{uniqueId}")
    public ResponseEntity fetchUseDetails(@PathVariable String uniqueId) {
        User user = userRepository.findByUniqueId(uniqueId);
        if (user != null) {
            return ResponseEntity.ok(user);
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("User not found");
        }
    }

    @RequestMapping("/userhome")
    public String userHome() {
        return "userHome";
    }
}
