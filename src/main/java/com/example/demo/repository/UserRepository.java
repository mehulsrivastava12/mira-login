package com.example.demo.repository;

import com.example.demo.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    User findByEmail(String email);
//    User findByEmailOrUserName(String email,String userName);
    User getByVerifyAccountToken(String token);
    User findByAccessToken(String token);
    User findByUniqueId(String uniqueId);

}
