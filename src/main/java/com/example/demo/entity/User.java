package com.example.demo.entity;

import lombok.*;
import javax.persistence.*;

@Data
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String userName;
    private String email;
    private String password;
    private String verifyAccountToken;
    private String accessToken;
    private boolean accountVerified;
    private String uniqueId;

    public boolean getAccountVerified() {
        return accountVerified;
    }
}

