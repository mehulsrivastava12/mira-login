package com.example.demo.util;

import org.springframework.stereotype.Component;

import java.nio.charset.StandardCharsets;
import java.util.Base64;

@Component
public class EncodeDecode {

    public String encodeBase64(String originalString) {
        // Encode a string to Base64
        byte[] encodedBytes = Base64.getEncoder().encode(originalString.getBytes(StandardCharsets.UTF_8));
        return new String(encodedBytes, StandardCharsets.UTF_8);
    }

    public String decodeBase64(String base64EncodedString) {
        // Decode a Base64-encoded string
        byte[] decodedBytes = Base64.getDecoder().decode(base64EncodedString.getBytes(StandardCharsets.UTF_8));
        return new String(decodedBytes, StandardCharsets.UTF_8);
    }
}
